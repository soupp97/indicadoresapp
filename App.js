import React from 'react';
import { StyleSheet, Platform} from 'react-native';
import { Provider as PaperProvider, Appbar } from 'react-native-paper';
import ListIndicators from './components/ListIndicators/index';
import Indicator from './components/Indicator/index';
import IndicatorHistorical from './components/IndicatorHistorical/index';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = () => (
  <PaperProvider >
    <Appbar.Header >
      <Appbar.Content title="IndicadoresApp" />
    </Appbar.Header>
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="IndicatorTable"
          component={ListIndicators}
          options={{ title: 'Lista de Indicadores' }}
        />
        <Stack.Screen
         name="Indicator"
         component={Indicator}
         options={({route}) => ({ title: route.params.name  })} />
        <Stack.Screen
         name="IndicatorHistorical"
         component={IndicatorHistorical}
         options={({route}) => ({ title: route.params.name  })} />
      </Stack.Navigator>
    </NavigationContainer>

    
  </PaperProvider>
);



export default App;
