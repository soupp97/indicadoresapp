import React from 'react';
import { useState, useEffect } from 'react';
import { StyleSheet, View, SafeAreaView, Text, Alert, ScrollView } from 'react-native';
import { Avatar, DataTable } from 'react-native-paper';
import { format, formatDistance, formatRelative, subDays } from 'date-fns'
import { es } from 'date-fns/locale';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const IndicatorHistorical = ({
    route
}) => {

    const [dataFetched, setDataFetched] = useState([])

    useEffect(() => {
        fetch('https://mindicador.cl/api/' + route.params.datos.codigo)
            .then((response) => response.json())
            .then((json) => {
                setDataFetched(json.serie);
            })
            .catch((error) => alert(error));
    });

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView>
                <DataTable>
                    <DataTable.Header>
                        <DataTable.Title><FontAwesome5 name={'calendar'} size={20} /></DataTable.Title>
                        <DataTable.Title numeric><FontAwesome5 name={'dollar-sign'} size={20} /></DataTable.Title>
                    </DataTable.Header>
                    {dataFetched.map((dato,i) =>
                        <DataTable.Row key={"indicador "+i}>
                            <DataTable.Cell>
                                {format(new Date(dato.fecha), 'dd MMMM yyyy', { locale: es })}
                            </DataTable.Cell>
                            <DataTable.Cell numeric>
                                <Text style={styles.title} >${dato.valor}</Text>
                            </DataTable.Cell>
                        </DataTable.Row>)}
                </DataTable>
            </ScrollView>

        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        margin: 16,
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,

    },
});


export default IndicatorHistorical;