import React, { useState, useEffect } from 'react';
import { StyleSheet, SafeAreaView, Text, Dimensions } from 'react-native';
import { Avatar, Card } from 'react-native-paper';
import { format} from 'date-fns'
import { es } from 'date-fns/locale';

import {
    LineChart
} from "react-native-chart-kit";


const LeftContent = props => <Avatar.Icon {...props} icon="finance" />

const Indicator = ({
    route
}) => {

    return (

        <SafeAreaView style={styles.container}>
            <Card>
                <Card.Title title={route.params.datos.codigo + ' - ' + format(new Date(route.params.datos.fecha), 'dd MMMM yyyy', { locale: es })} subtitle={route.params.datos.unidad_medida} left={LeftContent} />
                <Card.Content>
                </Card.Content>
                <Text style={styles.valor}>$ {route.params.datos.valor}</Text>

            </Card>
            <Card style={styles.grafico}>
                <Card.Content>
                        <LineChart
                            data={{
                                labels: route.params.labels,
                                datasets: [
                                    {
                                        data: route.params.data
                                    }
                                ]
                            }}
                            width={Dimensions.get("window").width -65} // from react-native
                            height={220}
                            yAxisLabel="$"
                            yAxisSuffix="k"
                            yAxisInterval={1} // optional, defaults to 1
                            chartConfig={{
                                backgroundColor: "#e26a00",
                                backgroundGradientFrom: "#fb8c00",
                                backgroundGradientTo: "#ffa726",
                                decimalPlaces: 2, // optional, defaults to 2dp
                                color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                                propsForDots: {
                                    r: "6",
                                    strokeWidth: "2",
                                    stroke: "#ffa726"
                                }
                            }}
                            bezier
                            style={{
                                marginVertical: 8,
                            }}
                        />
                </Card.Content>
            </Card>


        </SafeAreaView>
    )
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        margin: 16,
    },
    title: {
        textAlign: 'center',
        marginVertical: 8,

    },
    valor: {
        fontSize: 50,
        textAlign: 'center',
        margin: 20,
        color: 'blue'
    },
    grafico: {
        marginTop: 20
    }
});


export default Indicator;