import React from 'react';
import { useState, useEffect } from 'react';
import { StyleSheet, View, SafeAreaView, Text, Alert, ScrollView } from 'react-native';
import { Caption, DataTable, Subheading, Title, List, Modal, Button } from 'react-native-paper';
import { format } from 'date-fns'
import { es } from 'date-fns/locale';



const ListIndicators = ({
    navigation
}) => {

    const hideModal = () => setVisible(false);

    const [visible, setVisible] = useState(false);
    const [dataFetched, setDataFetched] = useState([])
    const [isLoading, setIsLoading] = useState(true);
    const [indicators, setIndicators] = useState();
    const [labelsChart, setLabelsChart] = useState([]);
    const [dataChart, setDataChart] = useState([]);

    const showModal = (dato) => {
        setVisible(true)
        setIndicators(dato)
        fetch('https://mindicador.cl/api/' + dato.codigo)
            .then((response) => response.json())
            .then((json) => {
                let fechas = []
                let valores = []
                json.serie.map((indicator, i) => {
                    if (i < 5) {
                        let fecha = format(new Date(indicator.fecha), 'dd MMM')
                        let dato = indicator.valor
                        fechas.push(fecha);
                        valores.push(dato);
                    }

                })
                setLabelsChart(fechas);
                setDataChart(valores);
            })
            .catch((error) => alert(error));


    };

    useEffect(() => {
        fetch('https://mindicador.cl/api')
            .then((response) => response.json())
            .then((json) => {
                let datosF = []
                datosF.push(json.bitcoin);
                datosF.push(json.uf);
                datosF.push(json.ivp);
                datosF.push(json.ivp);
                datosF.push(json.dolar);
                datosF.push(json.euro);
                datosF.push(json.ipc);
                datosF.push(json.utm);
                datosF.push(json.imacec);
                datosF.push(json.tpm);
                datosF.push(json.libra_cobre);
                datosF.push(json.tasa_desempleo);
                setDataFetched(datosF);
            })
            .catch((error) => alert(error));
    });







    return (

        <ScrollView >
            {dataFetched.map((dato, i) =>
                <List.Item
                    key={'indicador ' + i} onPress={() => showModal(dato)}
                    title={dato.nombre}
                    description={dato.unidad_medida}
                    left={props => <List.Icon {...props} icon="finance" />} />

            )}
            <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={styles.modal}>
                <Title style={styles.title}>¿Qué quieres ver?</Title>
                <Button style={styles.button} icon="finance" mode="contained" onPress={() => navigation.navigate('Indicator', { datos: indicators, name: indicators.nombre, labels: labelsChart, data: dataChart })}>Ver Detalles</Button>
                <Button style={styles.button} icon="calendar" mode="contained" onPress={() => navigation.navigate('IndicatorHistorical', { datos: indicators, name: indicators.nombre })}>Ver Historial</Button>
            </Modal>
        </ScrollView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        marginHorizontal: 16,
    },
    title: {
        textAlign: 'center',
        marginVertical: 12,

    },
    fixToText: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
    medida: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    modal: {
        padding: 20,
        marginHorizontal: 80,
        textAlign: 'center',
        backgroundColor: 'white',
        borderRadius: 20

    },
    button: {
        marginVertical: 8,
        textAlign: 'center'
    }
});


export default ListIndicators;